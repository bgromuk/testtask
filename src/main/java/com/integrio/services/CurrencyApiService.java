package com.integrio.services;

import com.integrio.jsonBeans.Day;
import com.integrio.jsonBeans.Rate;
import com.integrio.persistance.Currency;
import com.integrio.repository.CurrencyRep;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by bogdangromuk on 23.04.17.
 */

@Service
public class CurrencyApiService {
    public static final String USD = "USD";
    public static final String URL_PREFFIX_API_NBP_PL = "http://api.nbp.pl/api/exchangerates/tables/A/";
    @Autowired
    CurrencyRep rep;

    private static final Logger log = LoggerFactory.getLogger(CurrencyApiService.class);



    public void updateDbByDateRange(String startDate, String endDate) {
        log.info("Call to NBP API ranges " + startDate + " - " + endDate);
        RestTemplate restTemplate = new RestTemplate();

        Date apiStart = new Date();

        Day[] days = restTemplate.getForObject(URL_PREFFIX_API_NBP_PL + startDate + "/" + endDate + "/?format=json", Day[].class);
        Date apiEnd = new Date();
        Long diff = apiEnd.getTime() - apiStart.getTime();
        log.info("Used time for calling NBP api = " + diff);

        List<Currency> curs = new ArrayList<>();
        for (Day day : days
                ) {

            //System.out.println(day);
            for (Rate rate : day.getRates()) {
                if (rate.getCode().equals(USD)) {
                    Currency cur = new Currency(day.getEffectiveDate(), rate.getMid());
                    curs.add(cur);
                }
            }

        }
        rep.save(curs);
        log.info("Currency values inserted to DB with " + curs.size() + "values");
    }
}
