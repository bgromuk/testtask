package com.integrio.repository;

import com.integrio.persistance.Currency;
import org.springframework.data.domain.*;
import org.springframework.data.repository.*;

import java.util.Date;
import java.util.List;


/**
 * Created by bogdangromuk on 22.04.17.
 */

public interface CurrencyRep extends CrudRepository<Currency, Long> {

    Iterable<Currency> findByDateBetween(Date start, Date end);

}