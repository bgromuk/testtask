# README #

Testtask application to call NBP API for currency exchange values. Currency values from api stored in HSQLDB and showing on main page as Line Chart and Table.

### What is this repository for? ###

* Small single page app to monitor currency exchanges in date ranges.


### How do I get set up? ###

* Simply run "mvn spring-boot:run" command embeded tomcat will start automaticaly with HSQLDB.


### Who do I talk to? ###

* Bogdan Gromuk gromuk.bogdan@gmail.com
* skype: rabbit_6969