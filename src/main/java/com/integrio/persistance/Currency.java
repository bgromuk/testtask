package com.integrio.persistance;

/**
 * Created by bogdangromuk on 22.04.17.
 */
import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

@Entity
public class Currency implements Serializable {

    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false)
    private Date date;

    @Column(nullable = false)
    private Float mid;

    // ... additional members, often include @OneToMany mappings

    protected Currency() {
        // no-args constructor required by JPA spec
        // this one is protected since it shouldn't be used directly
    }

    public Currency(Date date, Float mid) {
        this.date = date;
        this.mid = mid;
    }

    public Date getDate() {
        return date;
    }

    public Float getMid() {
        return mid;
    }

    @Override
    public String toString() {
        return "Currency{" +
                "id=" + id +
                ", date=" + date +
                ", mid=" + mid +
                '}';
    }
}
