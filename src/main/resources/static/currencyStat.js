 function updateGraph(startDate, endDate) {

    $.get( "/currency/chart/"+startDate+"/"+endDate+"/", function( data ) {
        function getCurrencyChartData() {
            var currencies = [];

            fillTable(data);

            $.each(data, function( index, value ) {
                currencies.push({x: value.date, y: value.mid});
            });

            return [
                {
                    values: currencies,
                    key: "USD",
                    color: "#ff7f0e"
                }
            ];
        }

        nv.addGraph({
            generate: function() {
                var width = nv.utils.windowSize().width - 40,
                    height = nv.utils.windowSize().height - 40;
                var chart = nv.models.lineChart()
                    .margin({left: 100})  //Adjust chart margins to give the x-axis some breathing room.
                    .useInteractiveGuideline(true)  //We want nice looking tooltips and a guideline!
                    .showLegend(true)       //Show the legend, allowing users to turn on/off line series.
                    .showYAxis(true)        //Show the y-axis
                    .showXAxis(true)        //Show the x-axis
                ;

                //Format x-axis labels with custom function.
                chart.xAxis
                    .tickFormat(function(d) {
                      return d3.time.format('%x')(new Date(d))
                });

                chart.yAxis
                    .tickFormat(d3.format(',.2f'));

                d3.select('#usdChart')
                    .attr('width', width)
                    .attr('height', height)
                    .datum(getCurrencyChartData())
                    .call(chart);
                return chart;
            },
            callback: function(graph) {
                window.onresize = function() {
                    var width = nv.utils.windowSize().width - 40,
                        height = nv.utils.windowSize().height - 40,
                        margin = graph.margin();
                    if (width < margin.left + margin.right + 20)
                        width = margin.left + margin.right + 20;
                    if (height < margin.top + margin.bottom + 20)
                        height = margin.top + margin.bottom + 20;
                    graph.width(width).height(height);
                    d3.select('#test1')
                        .attr('width', width)
                        .attr('height', height)
                        .call(graph);
                };
            }
        });



    });
}

function updateDb() {
     $.get( "/currency/update/"+startDate+"/"+endDate+"/", function(data) {
        alert("DB updated");
     }).fail(function(data) {
        if (data.responseJSON.message) {
            alert( data.responseJSON.message );
        }
     });
}


 function fillTable(data) {
    var tbl_body = "";
    var odd_even = false;
    $.each(data, function() {
        var tbl_row = "";
        $.each(this, function(k , v) {
            if (k=="date") {
                var date = new Date(v);
                v = (date.getMonth() + 1) + '/' + date.getDate() + '/' +  date.getFullYear();
            }

            tbl_row += "<td>"+v+"</td>";
        })
        tbl_body += "<tr class=\""+( odd_even ? "odd" : "even")+"\">"+tbl_row+"</tr>";
        odd_even = !odd_even;
    })
    $("#currencyTbl tbody").html(tbl_body);
}