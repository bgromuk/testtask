package com.integrio;

import com.integrio.jsonBeans.Day;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class TesttaskApplication {

	public static void main(String[] args) {

        SpringApplication.run(TesttaskApplication.class, args);


    }
}
