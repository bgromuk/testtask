package com.integrio.controllers;

import com.integrio.jsonBeans.Day;
import com.integrio.jsonBeans.Rate;
import com.integrio.persistance.Currency;
import com.integrio.repository.CurrencyRep;
import com.integrio.services.CurrencyApiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 * Created by bogdangromuk on 22.04.17.
 * http://localhost:8080/currency/update/2011-01-01/2011-02-01
 */
@Controller
@RequestMapping("/currency")
public class CurrencyController {
    public static final String DATE_PATTERN = "yyyy-mm-dd";
    @Autowired
    CurrencyApiService apiService;

    @Autowired
    CurrencyRep rep;


    @RequestMapping(value="/update/{startDate}/{endDate}", method = RequestMethod.GET)
    public String loadDataToDb(@PathVariable("startDate") @DateTimeFormat(iso= DateTimeFormat.ISO.DATE) String startDate,
                               @PathVariable("endDate") @DateTimeFormat(iso= DateTimeFormat.ISO.DATE) String endDate) {
        apiService.updateDbByDateRange(startDate, endDate);
        return String.valueOf(rep.count());
    }

    @RequestMapping(value="/all/", method = RequestMethod.GET)
    public @ResponseBody Iterable<Currency> getChartData() {
        Iterable<Currency> all = rep.findAll();

        return all;
    }

    @RequestMapping(value="/chart/{startDate}/{endDate}", method = RequestMethod.GET)
    public @ResponseBody Iterable<Currency> getChartData(@PathVariable("startDate") @DateTimeFormat(iso= DateTimeFormat.ISO.DATE) Date startDate,
                               @PathVariable("endDate") @DateTimeFormat(iso= DateTimeFormat.ISO.DATE) Date endDate) {
        return rep.findByDateBetween(startDate, endDate);
    }
}
