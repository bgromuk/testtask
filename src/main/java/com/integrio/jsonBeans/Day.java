package com.integrio.jsonBeans;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Arrays;
import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Day {

    private String table;
    private String no;
    private Date effectiveDate;
    private Rate[] rates;

    public String getTable() {
        return table;
    }

    public void setTable(String table) {
        this.table = table;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public Date getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(Date effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public Rate[] getRates() {
        return rates;
    }

    public void setRates(Rate[] rates) {
        this.rates = rates;
    }

    @Override
    public String toString() {
        return "Day{" +
                "table='" + table + '\'' +
                ", no='" + no + '\'' +
                ", effectiveDate=" + effectiveDate +
                ", rates=" + Arrays.toString(rates) +
                '}';
    }
}
